// ==== CONFIGURATION ==== //

// Project paths
var src         = './src/',               // The raw material of your theme: custom scripts, SCSS source files, PHP files, images, etc.; do not delete this folder!
    build       = './',              // A temporary directory containing a development version of your theme; delete it anytime
    bower       = './bower_components/',  // Bower packages
    modules     = './node_modules/';      // npm packages

// Project settings
module.exports = {
  styles: {
    build: {
      src: src+'scss/style.scss',
      dest: build
    },
    compiler: 'libsass', // Choose a Sass compiler: 'libsass' or 'rubysass'
    cssnano: {
      discardComments: false,
      autoprefixer: {
        add: true,
        browsers: ['> 3%', 'last 2 versions', 'ie 9', 'ios 6', 'android 4'] // This tool is magic and you should use it in all your projects :)
      }
    },
    libsass: { // Requires the libsass implementation of Sass (included in this package)
      includePaths: [ // Adds Bower and npm directories to the load path so you can @import directly
        './src/scss',
        modules+'normalize.css',
        modules+'scut/dist',
        modules,
        bower,
      ],
      precision: 6,
      onError: function(err) {
        return console.log(err);
      }
    }
  }
}
